# README #

Roguelike игра, первоначально написанная мной на ассемблере 7 марта 2001.

Оригинальный исходник, nfo размером ровно 2 222 байта и скомпилированный для DOS COM-файл размером ровно 11 111 байт можно найти папке original_dos_asm_version.

В корне - кроссплатформенный ремейк на Arctic Engine.
Чтобы собрать и запустить этот ремейк нужно забрать репозиторий Arctic Engine https://gitlab.com/huldra/arctic.git в папку на том же уровне что и папка репозитория dreamland.
Например, у меня на рабочем столе рядом забраны папки arctic и dreamland и все собирается.
