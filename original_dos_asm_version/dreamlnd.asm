assume	cs:code,ds:code
	code segment
	org	100h
begin:
	mov	bx,0
	mov	cx,80*25
init_l1:
	mov	byte ptr caveseen+bx,0
	inc	bx
	loop	init_l1

	mov	ax,0003h
	int	10h
	mov	di,offset thy_name
	call	printf
	mov	text_x,12
	mov	di,offset thy_name
	call	inputef
	mov	text_x,0
	mov	di,offset text2
	call	printf
	mov	di,offset thy_name
	call	print
	mov	di,offset text3
	call	print
	mov	di,offset text4
	call	choose
	inc	text_y
	cmp	dx,1
	jl	target0
	je	target1
;target2
	mov	di,offset text5
	call	printf
	mov	di,offset thy_name
	call	print
	mov	di,offset text6
	call	print
	mov	have2_kill,1
	mov	have2_spell,1
	inc	text_y
	jmp	start;--------------------->
target0:
	mov	have2_kill,1
	jmp	start;>-------------->
target1:
	mov	have2_spell,1
start:;<-----------------------------X-----X
	mov	di,offset text7
	call	printf
	call	waitenter

	mov	bx,81
	call	mkm
next_step:;<------------------X
	call	pcave
	call	check_on_done
	cmp	game_done,1
	je	exit_game
	mov	ah,10h
	int	16h
	cmp	al,27
	je	end_of_programm;>---------->
	call	icka
	call	pcave
	jmp	next_step;>---^
exit_game:;--------------------------X
	mov	di,offset wakeup
	call	printf
	call	waitenter
end_of_programm:
	mov	ax,4c00h
	int	21h
;---------------------------------------------------------------------
;this procedure	prints 0-strings255 directly to	video RAM
;di - ofset of data
;ds - data segment
;bx - offset of	symbol on screen
;(ah - starting	text color)
print	proc
	push	0b800h
	pop	es
print_1:
	mov	al,byte	ptr ds:[di]
	cmp	al,0
	je	print_end
	cmp	al,255
	je	print_255
	cmp	al,247
	je	print_247
	mov	word ptr es:[bx],ax
	inc	bx
	inc	bx
	inc	di
	jmp	print_1
print_247:
	inc	text_y
	inc	di
	call	printf
	ret
print_255:
	inc	di
	mov	ah,byte	ptr ds:[di]
	mov	color,ah
	inc	di
	jmp	print_1
print_end:
	ret
	endp	print
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;just calls print AT text_x,text_Y ,color
printf	proc
	mov	bx,0
	mov	bl,text_y
	mov	ax,80
	mul	bl
	add	al,text_x
	shl	ax,1
	mov	bx,ax
	mov	ah,color
	call	print
	ret
	endp	printf
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;inputs	a 0-string
;ds:di - string	to input
;
input	proc
input_bgn:
	mov	ah,10h
	int	16h
	cmp	al,13
	je	input_ent
	mov	byte ptr ds:[di],al
	inc	di
	jmp	input_bgn
input_ent:
	mov	byte ptr ds:[di],0
	ret
	endp	input
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;inputs	a 0-string with	echo
;ds:di - string	to input
;
inpute	proc
	push	0b800h
	pop	es
inpute_ini:
	mov	cx,0
inpute_bgn:
	mov	ah,10h
	int	16h
	inc	cx
	cmp	al,13;enter
	je	inpute_ent
	cmp	al,8;backspace
	je	inpute_bsp
	mov	ah,color
	mov	byte ptr ds:[di],al
	mov	word ptr es:[bx],ax
	inc	di
	inc	bx
	inc	bx
	jmp	inpute_bgn
inpute_bsp:
	dec	cx
	jz	inpute_ini
	dec	cx
	dec	di
	dec	bx
	dec	bx
	mov	word ptr es:[bx],0000h
	jmp	inpute_bgn
inpute_ent:
	mov	byte ptr ds:[di],0
	ret
	endp	inpute
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;inpute	AT text_x,text_y
inputef	proc
	mov	bx,0
	mov	bl,text_y
	mov	ax,80
	mul	bl
	adc	al,text_x
	shl	ax,1
	mov	bx,ax
	call	inpute
	ret
	endp	inputef
;=====================================================================
;choose	procedure
choose	proc
choose_0:
	mov	dx,0
	push	di
choose_1:
	call	printf
	mov	ah,10h
	int	16h
	inc	cx
	cmp	al,13;enter
	je	choose_ent
	inc	di
	cmp	byte ptr ds:[di],0
	je	choose_mend
	inc	dx
	jmp	choose_1
choose_mend:
	pop	di
	jmp	choose_0
choose_ent:
	pop	di
	ret
	endp	choose
;---------------------------------------------------------------------
;Wait enter procedure (waits for space)
waitenter      proc
waitenter0:
		mov	ah,10h
		int	16h
		cmp	al,32
		jne	waitenter0
		ret
		endp	waitenter

;---------------------------------------------------------------------
;CLear Screen procedure
cls	proc
	mov	ax,0003h
	int	10h
	mov	text_x,0
	mov	text_y,0
	ret
	endp	cls
;---------------------------------------------------------------------
; printf and waitenter
printNwait	macro
		call	printf
		call	waitenter
		endm
;=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;Paintcave procedure
pcave	proc
	mov	cx,80*25
	mov	bx,0
	mov	di,0
	push	0b800h
	pop	es
pcave_put:
	cmp	byte ptr ds:[bx+offset caveseen],0
	je	pcave_nseen
	mov	al,ds:[bx+offset caveplan]
	cmp	al,'X'
	je	pcave_X
	cmp	al,'.'
	je	pcave__
	cmp	al,'D'
	je	pcave_D
	cmp	al,'T'
	je	pcave_T
	cmp	al,'G'
	je	pcave_G
	cmp	al,'E'
	je	pcave_E
	cmp	al,'B'
	je	pcave_B
	cmp	al,'M'
	je	pcave_M
	cmp	al,'*'
	je	pcave_STAR
	cmp	al,'R'
	je	pcave_R
	cmp	al,'L'
	je	pcave_L
	cmp	al,'-'
	je	pcave_minus
	cmp	al,'+'
	je	pcave_plus
	cmp	al,'i'
	je	pcave_i
	cmp	al,'d'
	je	pcave_i
	cmp	al,'1'
	je	pcave_1
	cmp	al,'p'
	je	pcave_p
	cmp	al,'W'
	je	pcave_W
	cmp	al,'u'
	je	pcave_u
	mov	ah,00fh
	jmp	pcave_draw
pcave_draw:
	mov	word ptr es:[di],ax
	inc	bx
	inc	di
	inc	di
	loop	pcave_put;-------------^

	mov	bx,cave_pos		;draw
	add	bx,bx			;player	on the map
	mov	ah,player_background
	mov	al,01h
	mov	word ptr es:[bx],ax  ;(white on	black)
	ret
pcave_nseen:
	mov	ax,0
	jmp	pcave_draw
pcave_X:
	mov	ax,007dbh	;wall
	jmp	pcave_draw	;white
pcave__:
	mov	ax,000dbh	;land
	jmp	pcave_draw	;black
pcave_D:
	mov	ax,00c02h	;dragon
	jmp	pcave_draw	;red
pcave_T:
	mov	ax,00a02h	;green
	jmp	pcave_draw	;troll
pcave_G:
	mov	ax,00e0fh	;yellow
	jmp	pcave_draw	;gold
pcave_E:
	mov	ax,074b1h	;white-brown
	jmp	pcave_draw	;Entrance
pcave_B:
	mov	ax,00201h	;dark-green
	jmp	pcave_draw	;Goblin
pcave_M:
	mov	ax,00f07h	;white
	jmp	pcave_draw	;Meat
pcave_STAR:
	mov	ax,019b1h	;blue
	jmp	pcave_draw	;water
pcave_R:
	mov	ax,00b01h	;blue
	jmp	pcave_draw	;magican
pcave_L:
	mov	ax,00201h	;dark-green
	jmp	pcave_draw	;Goblin-menyala
pcave_minus:
	mov	ax,002dbh	;green
	jmp	pcave_draw	;grass
pcave_plus:
	mov	ax,004dbh	;brown
	jmp	pcave_draw	;road
pcave_i:
	mov	ax,02a05h	;brightgreen on	darkgreen
	jmp	pcave_draw	;tree1
pcave_1:
	mov	ax,02a06h	;brightgreen on	darkgreen
	jmp	pcave_draw	;tree2
pcave_p:
	mov	ax,04c03h	;red on	brown
	jmp	pcave_draw	;princess
pcave_W:
	mov	ax,02e01h	;yellow	on green
	jmp	pcave_draw	;wiseman
pcave_u:
	mov	ax,02b04h	;blue on green
	jmp	pcave_draw	;bottle

	endp	pcave
;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;make move
;cave_pos  - old position
;bx  - new position
mkm  proc
	mov	al,byte	ptr ds:[bx+offset caveplan]
	cmp	al,'X'
	je	mkm_X
	cmp	al,'.'
	je	mkm__
	cmp	al,'D'
	je	mkm_D
	cmp	al,'T'
	je	mkm_T
	cmp	al,'G'
	je	mkm_G
	cmp	al,'B'
	je	mkm_B
	cmp	al,'M'
	je	mkm_M
	cmp	al,'*'
	je	mkm_STAR
	cmp	al,'R'
	je	mkm_R
	cmp	al,'L'
	je	mkm_L
	cmp	al,'-'
	je	mkm_minus
	cmp	al,'+'
	je	mkm_plus
	cmp	al,'p'
	je	mkm_princess
	cmp	al,'d'
	je	mkm_duplo
	cmp	al,'W'
	je	mkm_W
	cmp	al,'u'
	je	mkm_bottle
	ret
mkm_move:
	mov	cave_pos,bx
	mov	caveseen+80+bx,1
	mov	caveseen+bx,1
	mov	caveseen+bx+1,1
	mov	caveseen+bx-1,1
	mov	caveseen-80+bx,1
	call	pcave
mkm_X:
	ret
mkm__:
	mov	player_background,00fh
	jmp	mkm_move
mkm_D:
	call	meet_dragon
	ret
mkm_T:
	call	meet_troll
	ret
mkm_G:
	call	meet_gold
	ret
mkm_B:
	call	meet_goblin
	ret
mkm_M:
	call	meet_meat
	ret
mkm_STAR:
	cmp	have_boat,1
	je	mkm_STAR_1
	call	meet_water
	ret
mkm_STAR_1:
	mov	player_background,01fh
	je	mkm_move
mkm_R:
	call	meet_magican
	ret
mkm_L:
	call	meet_trader
	ret
mkm_W:
	call	meet_wiseman
	ret
	endp	mkm
mkm_minus:
	mov	player_background,02fh
	je	mkm_move
mkm_plus:
	mov	player_background,04fh
	je	mkm_move
mkm_princess:
	call	meet_princess
	ret
mkm_duplo:
	call	meet_duplo
	ret
mkm_bottle:
	call	meet_bottle
	ret

;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;in-cave keystroke analyser
;in al-ah - KeyCODE
icka	proc
	cmp	ah,50h
	je	icka_down
	cmp	ah,48h
	je	icka_up
	cmp	ah,4dh
	je	icka_right
	cmp	ah,4bh
	je	icka_left
	ret
icka_down:
	mov	bx,cave_pos
	add	bx,80
	call	mkm
	ret
icka_up:
	mov	bx,cave_pos
	sub	bx,80
	call	mkm
	ret
icka_left:
	mov	bx,cave_pos
	sub	bx,1
	call	mkm
	ret
icka_right:
	mov	bx,cave_pos
	add	bx,1
	call	mkm
	ret
	endp	icka

;---------------------------------------------------------------------
meet_dragon	proc
		mov	new_cave_pos,bx
		call	cls
		mov	di,offset dragon_1
		call	printf
		cmp	have_bad_sword,0
		je	meet_dragon_no_bsw
		mov	di,offset dragon_2
		call	choose
		inc	text_y
		cmp	dx,1
		jl	meet_dragon0;hit him with your sword
		;
		ret
meet_dragon0:
		cmp	sword_good,1
		je	meet_dragon_kill
		mov	di,offset dragon_3
		printNwait
		mov	have_bad_sword,0
		ret
meet_dragon_no_bsw:
		mov	di,offset dragon_2_1
		printNwait
		ret
meet_dragon_kill:
		mov	di,offset dragon_die
		printNwait
		mov	bx,new_cave_pos
		mov	byte ptr ds:[offset caveplan+bx],'.'
		mov	have_killed,1
		ret
		endp	meet_dragon
;---------------------------------------------------------------------
meet_troll	proc
		mov	new_cave_pos,bx
		call	cls
		mov	di,offset troll1
		call	printf
		cmp	have_bad_sword,1
		je	troll_hbs
		cmp	have_gold,1
		jge	 troll_hg
		mov	di,offset troll13
		printNwait
		ret
troll_hbs:
		cmp	have_gold,1
		jge	 troll_ha
		mov	di,offset troll2
		call	printf
		inc	text_y
meet_troll_0:
		mov	di,offset troll4
		printNwait
		mov	bx,new_cave_pos
		mov	byte ptr ds:[bx+offset caveplan],'.'
		ret
troll_hg:
		mov	di,offset troll2_1
		call	printf
		inc	text_y
		jmp	meet_troll_1
troll_ha:
		mov	di,offset troll2_1
		call	choose
		inc	text_y
		cmp	dx,1
		jE	meet_troll_0
		;have 2	give gold
meet_troll_1:
		mov	di,offset troll5
		mov	bx,new_cave_pos
		mov	byte ptr ds:[bx+offset caveplan],'.'
		printNwait
		mov	have_gold,0
		ret
		endp	meet_troll
;---------------------------------------------------------------------
meet_gold	proc
		mov	byte ptr ds:[offset caveplan+bx],'.'
		call	cls
		mov	di,offset meet_gold1
		add	have_gold,21
		printNwait
		ret
		endp	meet_gold
;---------------------------------------------------------------------
meet_goblin	proc
		mov	new_cave_pos,bx
		call	cls
		mov	di,offset goblin1
		call	printf
		mov	di,offset goblin2
		call	choose
		cmp	dx,1
		jl	meet_goblin0
		;donothing
		ret
meet_goblin0:
		cmp	have_meat,1
		jl	meet_goblin_nomeat
		mov	have_meat,0
		mov	gave_meat,1
		mov	di,offset goblin4
		printNwait
		mov	bx,new_cave_pos
		mov	byte ptr ds:[offset caveplan+bx],'.'
		ret
meet_goblin_nomeat:
		mov	di,offset goblin3
		printNwait
		ret
		endp	meet_goblin
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
meet_meat	proc
		mov	byte ptr ds:[offset caveplan+bx],'.'
		call	cls
		mov	di,offset meet_meat1
		add	have_meat,1
		printNwait
		ret
		endp	meet_meat
;=--0-==0==-0===-0==-0==-0==-0==-0==-0==-0==-0===-0==-00==0-=00-==0-==
meet_water	proc
		mov	ax,3
		int	10h
		mov	text_x,0
		mov	text_y,0
		mov	di,offset meet_water1
		printNwait
		ret
		endp	meet_water
;--=-=-==-==-=---==-=-===--=--===-=-=--=-=-==-==-=-==-=-=-===-=-==-==-
meet_magican	proc
		mov	new_cave_pos,bx
		call	cls
		mov	di,offset magican1
		call	printf
		mov	di,offset magican2
		call	choose
		cmp	dx,1
		je	meet_magican1
		ret
meet_magican1:
		inc	text_y
		cmp	have_gold,0
		jg	meet_magican2
		mov	di,offset magican3
		printNwait
		ret
meet_magican2:
		mov	have_gold,1
		mov	have_boat,1
		mov	di,offset magican4
		printNwait
		mov	bx,new_cave_pos
		mov	byte ptr ds:[offset caveplan+bx],'.'
		ret
		endp	meet_magican
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
meet_trader	proc
		mov	new_cave_pos,bx
		call	cls
		mov	di,offset trader1
		call	printf
		cmp	gave_meat,1
		je	meet_trader2
		mov	di,offset trader2
		call	choose
		inc	text_y
		cmp	dx,1
		je	meet_trader3
meet_trader0:
		ret
meet_trader3:
		cmp	have_meat,1
		je	meet_trader_wm
		mov	di,offset trader6
		printNwait
		ret
meet_trader_wm:
		mov	di,offset trader3
		printNwait
		mov	bx,new_cave_pos
		mov	byte ptr ds:[offset caveplan+bx],'.'
		mov	have_meat,0
		mov	have_boat,1
		ret
meet_trader2:
		mov	di,offset trader4
		call	choose
		inc	text_y
		cmp	dx,1
		jl	meet_trader0
		je	meet_trader3
		mov	di,offset trader5
		printNwait
		mov	bx,new_cave_pos
		mov	byte ptr ds:[offset caveplan+bx],'.'
		ret
		endp	meet_trader
;---------------------------------------------------------------------
meet_princess	proc
		mov	new_cave_pos,bx
		call	cls
		mov	di,offset princess1
		call	printf
		cmp	have_bottle,1
		je	meet_princess_hb
		mov	di,offset princess2
		call	choose
		cmp	dx,1
		jl	meet_princess_kiss
		ret
meet_princess_kiss:
		mov	di,offset princess_kiss
		printNwait
		ret
meet_princess_hb:
		mov	di,offset princess3
		call	choose
		cmp	dx,1
		jl	meet_princess_kiss
		jg	meet_princess_bottle
		ret
meet_princess_bottle:
		mov	di,offset princess_bottle
		printNwait
		mov	have_spelled,1
		mov	bx,new_cave_pos
		mov	byte ptr ds:[offset caveplan+bx],'+'
		ret
		endp	meet_princess
;=====================================================================
meet_duplo	proc
		call	cls
		mov	di,offset duplo1
		call	printf
		mov	di,offset duplo2
		call	choose
		cmp	dx,1
		jl	meet_duplo0
		je	meet_duplo1
		cmp	dx,2
		je	meet_duplo2
		ret
meet_duplo0:
		mov	di,offset duplo_0
		printNwait
		ret
meet_duplo1:
		mov	di,offset duplo_1
		printNwait
		ret
meet_duplo2:
		cmp	have_bad_sword,0
		je	meet_duplo_nsw
		mov	di,offset duplo_2
		printNwait
		mov	sword_good,1
		ret
meet_duplo_nsw:
		mov	di,offset duplo_2_nsw
		printNwait
		ret
		endp	meet_duplo
;---------------------------------------------------------------------
meet_wiseman	proc
		call	cls
		inc	wiseman_visitnum
		cmp	wiseman_visitnum,2
		jl	meet_wise_1
		je	meet_wise_2
		cmp	wiseman_visitnum,4
		jl	meet_wise_3
		je	meet_wise_4
		mov	wiseman_visitnum,0
		mov	di,offset wiseman5
		printNwait
		ret
meet_wise_1:
		mov	di,offset wiseman1
		printNwait
		ret
meet_wise_2:
		mov	di,offset wiseman2
		printNwait
		ret
meet_wise_3:
		mov	di,offset wiseman3
		printNwait
		ret
meet_wise_4:
		mov	di,offset wiseman4
		printNwait
		ret
		endp	meet_wiseman
;---------------------------------------------------------------------
meet_bottle	proc
		mov	byte ptr ds:[offset caveplan+bx],'-'
		call	cls
		mov	di,offset bottle
		printNwait
		mov	have_bottle,1
		ret
		endp	meet_bottle
;=====================================================================
check_on_done	proc
		cmp	have2_kill,1
		jne	check_nokill
		cmp	have2_spell,1
		jne	check_nospell
		cmp	have_killed,1
		jne	check_over
		cmp	have_spelled,1
		jne	check_over
		mov	game_done,1
		;princess and dragon!!!
		call	cls
		mov	di,offset done_both
		printNwait
		ret
check_nokill:
		cmp	have_spelled,1
		jne	check_over
		mov	game_done,1
		;princess only!!
		call	cls
		mov	di,offset done_princess
		printNwait
		ret
check_nospell:
		cmp	have_killed,1
		jne	check_over
		mov	game_done,1
		;dragon	only!!
		call	cls
		mov	di,offset done_dragon
		printNwait
		ret
check_over:	ret
		endp	check_on_done
text_x	db	0
text_y	db	0
color	db	8ah
have2_kill	db	0
have2_spell	db	0
have_killed	db	0
have_spelled	db	0
have_bad_sword	db	1
sword_good	db	0
have_gold	db	0
have_meat	db	0
have_boat	db	0
gave_meat	db	0
have_bottle	db	0
game_done	db	0
player_background	db	0
wiseman_visitnum	db	0
cave_pos  dw	  80*1+1
new_cave_pos	dw	?
thy_name	db	255,008h,'kom�u�bHy� koDep Bu� �uc�a '
		db	??Date,' B ',??Time,' (BpeM� MockoBckoe)',247,247
		db	255,00ah,'- �� �����?',247
		db	255,00fh,'- ���� ���� ',0
text2	db	247,255,00ah,'- ��, ��ࠢ���, ',0
text3	db	'. ��祬 �� ��襫 �?',247,255,00fh,0
text4	db	'- � ��襫 㡨�� ��୮�� �ࠪ���.     ',0
	db	'- ���� �᪮������� �ਭ���� �������.',0
	db	'- ���� ࠤ� �ਪ���...              ',0,0
text5	db	255,00ah,'- ��� �� ��� ������� �� �ਪ������! ���! � �ॢ��� ⥡� � ����!',247
	db	255,00fh,'- �⮩�! ����� �� ����, �?',247
	db	255,00ah,'- ����, ',0
text6	db	', ����... �� ⥡� �� ࠢ�� �������� ⮫��!',247
	db	255,00fh,'- ��� ��������?! � ��୮�� �ࠪ��� ���� �������!!!',247
	db	255,00ah,'- ���... � �� ��, �� �� ������? �������...',247
	db	255,00fh,'- ��������, �� ����!!',247
	db	255,00ah,'- �����, 㣮��ਫ. � �� ��� �ॢ���� ⥡� � ����, �᫨ �� ���� �ࠪ��� �',247
	db	'  �᪮����� �ਭ���� �������.',0
text7	db	255,007h,'�࠭�⥫� ���������� ���� ���� � �� ��諨 � ⥬��� �����...',247
	db	'����, �� �� �ᥣ�� ���� � ᮡ�� 䠪�� � ������! ������ ����� ��祭��, �',247
	db	'�� 㢨����, �� �⮨� � ������� ��ਤ��. �࠭�⥫� �������� ��� ����',247
	db	'���孨� ������ � ����� �� ���� ����...',0
dragon_1	db	255,00ch,'��। ���� �� ���� ����� ����� ���� �ࠪ��. ��� ������� - �� ᯨ�.',247,0
dragon_2	db	'�� 㤠��� ��� ��箬.         ',0
dragon_2_1	db	'�� �蠥� ���� �� �ண��� ���.',0,0
dragon_3	db	'��� ��� �������� �� ��� ⮫���� ����. ���� ���� �� ���㫠��!!',0
dragon_die	db	'��� ��� ���몠�� ⮫���� ���� �ࠪ��� � ��������� ��� ��אַ � ���!',247
		db	'����� �᪠������, ॢ��... � �� ��� ᭮�� ��箬...',247
		db	'��᫥ ���� �ᮢ ��祭�� ���� �ࠪ�� ����!',0
troll1	db	255,00ah,'��। ���� ��⪨� �஫��. � �ࠢ�� �ࠡ�� � ���� ���.',247
	db	'- ��蠫� ��� �����?! �롥ࠩ!',247,0
troll2_1	db	'�� �⤠�� �ࠡ�⥫� ��� ᢮� �����...                   ',0
troll2		db	'�� ���⠥� ᢮� ���, �⮡� ������ ������� �⮬� 㡫�.',0,0
troll4	db	'�஫�� ����� ���ࠧ����騩 �ਪ, ᪮�稫 ������ ஦� � ����� � �������⭮�',247
	db	'���ࠢ����� � ���ࠢ���������� ᪮�����',247,0
troll5	db	'�஫�� ��� ��� ������� �, ��ᢨ��뢠� ������� ��堡��� ��ᥭ��, ��襫 ��',247
	db	'�ய����� � ��������� ⠢���.',247,0
troll13	 db	 '����� � ��� ᮢᥬ �� ��⠫���, � 祬 �� ��⭮ �ਧ������ �ࠡ�⥫�. �஫��',247
	db	'���᪠� ���, �, ����⢨⥫쭮 �� ����� �����, �⢥ᨫ ��� ᬠ筮�� ������.',247
	db	'� ��� ��������� �� ����� ��-� �த� ''������, ���''...',247,0
meet_gold1	db	255,00eh,'�� ��諨 21 ����⮩!',0
goblin1	db	255,002h,'��। ���� ����让-�ॡ���让 ������ ������.',247
	db	'- ���, ��, ��������-������! ��� ������! ��� 㬭�! ���� ���� �ன���, �',247
	db	'  ��� ⢮� �� ��᪠��, �-�, �-�! ���� ᯥࢠ ���� ��� ��᮪ ᠫ�, ��� ⢮�',247
	db	'  ⮫쪮 ⮣�� ��᪠��, ����� ����, �-�! ��� ������-������, ������, ��� �',247
	db	'  㬭�-㬭�, ����-����...',247,0
goblin2	db	'�� �蠥� ���� ������� ᠫ�.        ',0
	db	'�७ ���, � �� ᠫ�! �����, ⮦� ���!',0,0
goblin3	db	247,'�����⢥���� �஡���� - � ��� ���� � ᮡ�� ᠫ�!',0
goblin4	db	247,'- ���ᨡ�-ᯠᨡ�! - ᪠��� ������. ��⮬ �� ����⨫, ��� �� �� ���� ᬮ���,',247
	db	'  � ������� - ���� �㬠��, ��� �� ��������? ���� ���� �㬠�� � ���! ��� ����!',247
	db	'��᫥ �⮣�, �룠� � ������� ��堡�� ��ᥭ��, ������ �襫 �७ ����� �㤠.',0
meet_meat1	db	255,00fh,'�� ��諨 㢥���� 謠� ᠫ�!',0
meet_water1	db	255,009h,'�� �⮨� �� ��ॣ� ��������� ४�. � ⥬��� 宫����� ���� ��।�� �஭������',247
		db	'⥭� ᫥��� ��.',0
magican1	db	255,00bh,'�� ����⨫� ������� ����, ����⨢襣��� �� ⮣�, �⮡� ����� �த����� ᢮�',247
		db	'���襡�� ᢨ⪨. ����� ������� ��������� �।������ ��� �㯨�� � ����',247
		db	'���������� "�� ���� ��� ������" �� "ᯥ樠�쭮� 業�".',247
		db	'����� � ����, �� ���� �⫨����� ᯮᮡ������ ��।����� ������⢮ ����� �',247
		db	'��ଠ��� ᢮�� �����⮢, ⠪ �� "ᯥ樠�쭠� 業�" ᪮॥ �ᥣ� ᮢ����� �',247
		db	'�⨬ ᠬ� ������⢮�!',247,0
magican2	db	'��� �� �祭� ���, �� ��� �� �������� ������� ����������.',0
		db	'�� ���⥫� �㯨�� ��� ᢨ⮪.                               ',0,0
magican3	db	'�����, �� � ��� ᮢᥬ ��� �����!',0
magican4	db	'��� � ᫥������ �������, ��� ��ᬮ�५ � ���襡��� ��ઠ��, ���ᠫ ९� �',247
		db	'�।����� ��� �� ������ ��⤠�� �� �����, ����� � ��� ���� � ��襫쪥.',247
		db	'�� ⠪ � ᤥ����... ��� �⤠� ��� ᢨ⮪ � ��襫 �ய����� ��ࠡ�⮪ � ���������',247
		db	'⠢���. ����� ����, �� �� ��������� ��ᬮ����, ��� �� ����� � ���� ��᪠�.',247
		db	'����, ������ �� � �⫨砥� �㫨��-���� �� �㫨��-�஫��! ����, � ���',247
		db	'��⠫�� ஢�� ���� ����⮩.',0
trader1	db	255,002h,'��। ���� ������. ��� � �� �������, �� ����让, ������ � 㬭�.',247
	db	'- ���! � ��� � �㬠�� ���� � �� 室���! ��� ��� ����ந���! ���蠩, ���',247
	db	'  �।������ ⢮� �����! ��� ࠭��e ������ 訫� �� �뫮. �뫮 �� �����! ���',247
	db	'  㬭�-㬭�! ��� ����� ⠪ �� ⮣�! ��� ⥯��� ����� ����� �� ᠫ�! � ⢮�',247
	db	'  ���� ᠫ�?',247,0
trader2	db	'- ���, ���� � ���� ᠫ�!',0
	db	'- ����� �������!        ',0,0
trader3	db	'�� �⤠�� ������� ᠫ�, �� �⤠� ��� ����� �, ������� ⠪�� ��ᥭ��, �� ������',247
	db	'� ��ଠ���� �� �� � ��㡮�� ᢮�稢�����, ��襫 ���� ᠫ�.',0
trader4	db	'- ���, ���� � ���� ᠫ�!                       ',0
	db	'- ����� �������!                               ',0
	db	'- ����, � � �� ᠫ� 㦥 �⤠� ⢮��� �த���!',0,0
trader5	db	'���蠢 ��, ������ ����� ࠤ���� ��, �墠⨫ ����� ��� ���� � 㡥���.',0
trader6	db	'- ���� ᯥࢠ �������� ᠫ�, ��� ��⮬ ������ �����!',247
	db	'C��� � ��� ���, ⠪ �� ��祣� �� ����稫���...',0
princess1	db	255,00ch,'��। ���� ������������� �ਭ��� ������.',247,0
princess2	db	'�� 楫�� �� � ���.',0
		db	'�� ��祣� �� ������.',0,0
princess3	db	'�� 楫�� �� � ���.                 ',0
		db	'�� ��祣� �� ������.                 ',0
		db	'�� ������� � ��� ���� ᯨ�� �� �.',0,0
princess_kiss	db	247,'���... ���⭮... ⮫쪮 ��� ��� �� ���㫠��.',0
princess_bottle	db	247,'�ਭ��� ������ ��㫠�� �� ������ ��嬥���, ��⠫� � ���⥫�, �������� ���',247
		db	'� ᥡ� ����稪��, �, ����� �� ����諨 �������, 祬-� � �ᥩ ��� ������㫠 ��',247
		db	'��誥 � �� �⪫�稫���. ��१ �� �� ��㫨�� �冷� � �������஬ �� ����.',0
duplo1	db	255,00ah,'�� �⮨� ��। ��ॢ�� � ��஬�� �㯫��.',247,0
duplo2	db	'�� ��� � �㯫� �㪮�.                                     ',0
	db	'�� ��� � �㯫� ������ �⮡� ��ᬮ����, �� ⠬ �ந�室��.',0
	db	'�� ����� � �㯫� ��箬.                                    ',0
	db	'�� ���, �� �㯫�, ��䨣!                                    ',0,0
duplo_0	db	247,'����...',247,'...���...',247,'...�� ��祣� �� ���ਢ���.',0
duplo_1	db	247,'���७� �� �����. ����� �祭�.',0
duplo_2	db	247,'����஢�� � ����� �� 㡥�������, �� � �㯫� ���� ���७�.',0
duplo_2_nsw	db	247,'��筥�, ��� ����஢��� ⠬ ��箬... � ���-� � ��� � ����!',0
wiseman1	db	255,00eh,'���� ���� ��襫쭨� � ���� ����� �������� ��� ����ﬨ.',247
		db	'�� ���� �࠯��. ������ �࠯ ������ ��譮 ��-� �த�:',247
		db	'  - ��祭�� ����誠... ᮫��� �����... �������...  ���, ���� ����窨...',0
wiseman2	db	255,00eh,'���� ���� ��襫쭨� � ���� ����� �������� ��� ����ﬨ.',247
		db	'�� ���譮 �࠯��, � �� ���� ���� ࠧ�� ᯨ���. �� ᭥ �� ��ମ�� ��-� ��',247
		db	'⮣� �����⮩���, �� � �� ������ ��� �� ��������.',0
wiseman3	db	255,00eh,'���� �७ ����ᠥ� ��� ����ﬨ � ���� �����. �� ᯨ�.',247
		db	'�� ᭥ ��ମ�� ��-� � ��஫� � ��� ����誥, � ��-� � ��襩 ����誥 ⮦�!!',0
wiseman4	db	255,00eh,'���� ࠧ������ �࠯��, � ����� ����� ������ � ������. ���譮 ᫮��:',247
		db	'  - �ࠪ��� 㡨�� �⮡� ��� �㦭� � �㯫� ������ ⮣� �㡠, �� ����!',247
		db	'    � ���� �㦭� �᪮������� �ਭ���� �⮡� �ਭ��� �� ���� �� ���⮪�.',0
wiseman5	db	255,00eh,'��ਪ �࠯�� �����:',247
		db	'  - ����� �� �� �� ���஢�, �� ���।� ����, �� ��⠥� ஢ �����, ���',247
		db	'    ᯨ� �ਭ���, ������ ���������� ���� ���, ����� 㬥� ��� �����, �����!',0
bottle	   db	   255,00bh,'�� ��諨 ��� � ᯨ�⮬!!!',0
done_princess	db	255,00ah,'  - �� ���, �� � �᪮������ �ਭ����!',255,007h,' - ࠧ����� ������� �����.',247
		db	255,00fh,'  - ��...',247
		db	255,00ah,'  - ��-� �� �� �ᮡ� �������... �� �, ����, � ���� ⥡� ⠩��?',247
		db	255,00fh,'  - ����筮!',0
done_dragon	db	255,00fh,'  - �����!!! � �ࠧ�� ������!!!',247
		db	255,00ah,'  - ���, �訥, �� ���, �� ���� ⠪ �㬥�� � ����!',247
		db	255,00fh,'  - �, �� ��, �࠭�⥫�! ����� ��� � ���?',0
done_both	db	255,00fh,'  - �����-��� �࠭�⥫�!?',247
		db	255,00ah,'  - �! � ����, �� 㦥 �� ᤥ���! �������! ����� �� ⥡� �멤�� ����!',247
		db	255,00fh,'  - �!!! �࠭�⥫�!!! ����� ����?! �� �� ������ਫ���!!!',0
wakeup	db	247,255,007h,'� ���� �� ���㫨��...��-��, �� �� ⮫쪮 ᮭ! �� ᠤ���� �� �஢�� �',247
	db	'���� ��������, �� ᦨ���� ��-� � �㪥. �, ��ᯮ��! �� �� ������ �����!',247
	db	'�� ᠬ�� ������ ����� �� ᭠, ������ �� �⠨�� �� ����! ���⨪�!!!',0
;
; E - ������������ ������
; X - ������

; . - ������ ���
; - - ������� ���
; + - ���������� ���

; * - ����	�㦭� ����� ���	�������� �� ����
; G - ������	21 �������
; M - ����
; u - �����

; W - ���������	���� ������樨	� ���� � ������

; d - ���	�᫨ ����� � �㯫� ���, � �� �⠭�� ���襡��
; B - ������	���� ���� � �室��
; D - ������	������ ����� ��� // 㬨ࠥ� �� ���襡���� ����
; T - ������	���� ���->�室�� // ���� ��� ������ � �室��
; R - ���	��� ������->1 ������� +	����������(�����) + �室��
; L - ������	���ଠ�� � ������� � ����->�室�� // ����->�����+�室��
; p - ���������	��� ���� ������� �㦥� �����
;
caveplan	db	'XEXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXiiii1i1ii1i1ii1i11i1ii11111i1iiiiii'
		db	'X.X....XXXXX...XXX...XXX.XXX.XXXXXXXXXXXXXXX----1---11i--i--1----i1--iiii11--i-i'
		db	'X...**...XX.....DX.X.XX...X...XXXXXXXXXXXX---1--ii1--i-i-++++++++++++-1-----i--i'
		db	'XX*****X....XXX....X.X..X.XXX....XXXXXXX-1---------------+-*****----+---****--ii'
		db	'XXX***XXXLXXXXXXXX.....XX..XT.XX..X.XXXX----------1----**.***XX*****.****XX*---i'
		db	'XXXX**XX....X..X.X.XXXXXXX...XXX.XX.XX-------i-----*****-+--*XXXXXXX+XXXXXX*i-i1'
		db	'XXX**XXX.XX...X.....XX..XX.X..XM....X----1-------***1----+--**X++++++++++X**---i'
		db	'XX**XX...TXXX..X.XX......XX.XXXXXXXXXXX----------*------++--i*XXXXXXX++++X*i-i11'
		db	'X**XXXXXX.XXXX.XXX..XX............X----------i---**---+++----*X+++++X++++X*---ii'
		db	'XX**X***X....XXX.....XXXXX.XX..XX.XXXX--1--------i**--+i----i*Xp+X+++++++X**i--i'
		db	'XXX***X***.*..XXXX.X....X..XXX...XXX------------+++.+++-----*XXXXXXXXXXXXXX*---i'
		db	'XXXXXXXX*****....XXXXXX.XXXX.XXX.B.XXXX----i----+--***-----1*XX**********XX*i-ii'
		db	'XX..XXXXX***XXXX.X...X....X....XXX....XXX-------+---i**-----***i-----i--****-i-i'
		db	'XX.XXXXXXXXXXX.....X..X.X....XGX.XX.X..X--------+-----**------------------i---11'
		db	'XX....XXX......XX.XXXXX.X.XXXXXX....*XXXXXXXX--i+-----1**-----i-------------i--i'
		db	'XXXX......X.XX.XX...XX..XXXXX..XX.***X**...XXX--+1------*-----------i--1-----1ii'
		db	'XXXX.XXX.XXXXX.XXXX...XXX***....XX.****XXX....+++---i---*---i-----1--i---i--i--1'
		db	'XXR...XX.XXXX..XXXXXXXXXX*XX...XXXXX**XXXXXXXX-----i---**----------------1i---ii'
		db	'XX**.*XX..X......XX.XX****XXXXXX...X.XXX-i------i---1*****--1--i--i--i--i-i-i1ii'
		db	'X*****XXX...XX.X.....**X*XX--XX..X...XX------1---1-***--****--1--i-1-1i-i-i1ii11'
		db	'XX***XXXXX***X.XX.XXX*XXX-----XXXXXXXXXX---------****--d-****---i-1-i-ii---1--ii'
		db	'XXX***XX*****..XX.X***XX-----------XXXX-i---1-i-******--****-i---i------1----ii1'
		db	'XXXX****XX***X....XX***XX------------------------*1*******-i-i-i---i-1----i1---i'
		db	'XXXXXXX****XXXX******XXXW--XX--XX-X-X-i----i-i--1ii1---ii-ii-i--i-ii-i11ii---iu1'
		db	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXi1ii1i1i1i1ii11i11i1i1i1ii1i1ii1i1i11ii'
caveseen	db	80*25 dup(?)
code	ends
	end	begin
