﻿#include <unordered_map>
#include "engine/easy.h"
#include "engine/unicode.h"

using namespace arctic;  // NOLINT

std::vector<std::string> vec = {
  u8"?", u8"☺", u8"☻", u8"♥", u8"♦", u8"♣", u8"♠", u8"•", u8"◘", u8"○", u8"◙", u8"♂", u8"♀", u8"♪", u8"♫", u8"☼",
  u8"►", u8"◄", u8"↕", u8"‼", u8"¶", u8"§", u8"▬", u8"↨", u8"↑", u8"↓", u8"→", u8"←", u8"∟", u8"↔", u8"▲", u8"▼",
  u8" ", u8"!", u8"\"", u8"#", u8"$", u8"%", u8"&", u8"'", u8"(", u8")", u8"*", u8"+", u8",", u8"-", u8".", u8"/",
  u8"0", u8"1", u8"2", u8"3", u8"4", u8"5", u8"6", u8"7", u8"8", u8"9", u8":", u8";", u8"<", u8"=", u8">", u8"?",
  u8"@", u8"A", u8"B", u8"C", u8"D", u8"E", u8"F", u8"G", u8"H", u8"I", u8"J", u8"K", u8"L", u8"M", u8"N", u8"O",
  u8"P", u8"Q", u8"R", u8"S", u8"T", u8"U", u8"V", u8"W", u8"X", u8"Y", u8"Z", u8"[", u8"\\", u8"]", u8"^", u8"_",
  u8"`", u8"a", u8"b", u8"c", u8"d", u8"e", u8"f", u8"g", u8"h", u8"i", u8"j", u8"k", u8"l", u8"m", u8"n", u8"o",
  u8"p", u8"q", u8"r", u8"s", u8"t", u8"u", u8"v", u8"w", u8"x", u8"y", u8"z", u8"{", u8"|", u8"}", u8"~", u8"⌂",
  u8"А", u8"Б", u8"В", u8"Г", u8"Д", u8"Е", u8"Ж", u8"З", u8"И", u8"Й", u8"К", u8"Л", u8"М", u8"Н", u8"О", u8"П",
  u8"Р", u8"С", u8"Т", u8"У", u8"Ф", u8"Х", u8"Ц", u8"Ч", u8"Ш", u8"Щ", u8"Ъ", u8"Ы", u8"Ь", u8"Э", u8"Ю", u8"Я",
  u8"а", u8"б", u8"в", u8"г", u8"д", u8"е", u8"ж", u8"з", u8"и", u8"й", u8"к", u8"л", u8"м", u8"н", u8"о", u8"п",
  u8"░", u8"▒", u8"▓", u8"│", u8"┤", u8"╡", u8"╢", u8"╖", u8"╕", u8"╣", u8"║", u8"╗", u8"╝", u8"╜", u8"╛", u8"┐",
  u8"└", u8"┴", u8"┬", u8"├", u8"─", u8"┼", u8"╞", u8"╟", u8"╚", u8"╔", u8"╩", u8"╦", u8"╠", u8"═", u8"╬", u8"╧",
  u8"╨", u8"╤", u8"╥", u8"╙", u8"╘", u8"╒", u8"╓", u8"╫", u8"╪", u8"┘", u8"┌", u8"█", u8"▄", u8"▌", u8"▐", u8"▀",
  u8"р", u8"с", u8"т", u8"у", u8"ф", u8"х", u8"ц", u8"ч", u8"ш", u8"щ", u8"ъ", u8"ы", u8"ь", u8"э", u8"ю", u8"я",
  u8"Ё", u8"ё", u8"Є", u8"є", u8"Ї", u8"ї", u8"Ў", u8"ў", u8"°", u8"∙", u8"·", u8"√", u8"№", u8"¤", u8"■", u8" "};

struct Symbol {
  Ui32 code;
  Rgba fore_color;
  Rgba back_color;
};

Symbol text_buf[80*25];
std::unordered_map<char32_t, Sprite> g_ch;
std::vector<char32_t> g_ascii;


Si32 text_x = 0;
Si32 text_y = 0;
Rgba text_fore_color = Rgba((Ui32)0xffffffff);
Rgba text_back_color = Rgba((Ui32)0x000000ff);
Ui8 color = 0x8a;
Ui8 have2_kill = 0;
Ui8 have2_spell = 0;
Ui8 have_killed = 0;
Ui8 have_spelled = 0;
Ui8 have_bad_sword = 1;
Ui8 sword_good = 0;
Ui8 have_gold = 0;
Ui8 have_meat = 0;
Ui8 have_boat = 0;
Ui8 gave_meat = 0;
Ui8 have_bottle = 0;
Ui8 game_done = 0;
Ui8 player_background = 0;
Ui8 wiseman_visitnum = 0;

Ui16 cave_pos = 80*1+1;
Ui16 new_cave_pos;

char thy_name[128] = {0};
char welcome[] = u8"◑\x08komПuЛbHyЛ koDep Buй ЧucЛa " __DATE__
u8" B " __TIME__ u8" (BpeMя MockoBckoe)\n\n"
u8"◑\x0a- Кто здесь?\n"
u8"◑\x0f- Зови меня \0";
char text2[] = u8"\n◑\x0a- Ну, здравствуй, \0";
char text3[] = u8". Зачем ты пришел сюда?\n◑\x0f\0";
char text4[] = u8"- Я пришел убить Черного Дракона.     \0"
u8"- Хочу расколдовать принцессу Танагру.\0"
u8"- Просто ради прикола...              \0\0";
char text5[] = u8"◑\x0a- Как же мне надоели эти приколисты! Всё! Я превращу тебя в жабу!\n"
u8"◑\x0f- Стойте! Может не надо, а?\n"
u8"◑\x0a- Надо, \0";
char text6[] = u8"', надо... От тебя все равно никакого толку!\n"
u8"◑\x0f- Как никакого?! Я Черного Дракона могу замочить!!!\n"
u8"◑\x0a- Хмм... и это все, что ты можешь? Маловато...\n"
u8"◑\x0f- Пожалуйста, не надо!!\n"
u8"◑\x0a- Ладно, уговорил. Я не буду превращать тебя в жабу, если ты убьешь Дракона и\n"
u8"  расколдуешь принцессу Танагру.\0";
char text7[] = u8"\n◑\x07Хранитель подземелья открыл ворота и вы вошли в темную пещеру...\n"
u8"Хорошо, что вы всегда носите с собой факел и огниво! Десять минут мучений, и\n"
u8"вы увидели, что стоите в длинном коридоре. Хранитель подмигнул вам левым\n"
u8"верхним глазом и закрыл за вами ворота...\0";
char dragon_1[] = u8"◑\x0cПеред вами на полу пещеры лежит Черный Дракон. Вам повезло - он спит.\n\0";
char dragon_2[] = u8"Вы ударяете его мечом.         \0"
u8"Вы решаете пока не трогать его.\0\0";
char dragon_2_1[] = u8"Вы решаете пока не трогать его.\0\0";
char dragon_3[] = u8"Ваш меч ломается об его толстую шкуру. Зверюга даже не проснулась!!\0";
char dragon_die[] = u8"Ваш меч протыкает толстую шкуру дракона и впивается ему прямо в сердце!\n"
u8"Зверь вскакивает, ревет... а вы его снова мечом...\n"
u8"После двух часов мучений Черный Дракон мертв!\0";
char troll1[] = u8"◑\x0aПеред вами жуткий тролль. В правой грабле у него нож.\n"
u8"- Кашалёк али жисть?! Выберай!\n\0";
char troll2_1[] = u8"Вы отдаете грабителю всё своё золото...                   \0"
u8"Вы достаете свой меч, чтобы надрать задницу этому ублюдку.\0\0";
char troll2[] = u8"Вы достаете свой меч, чтобы надрать задницу этому ублюдку.\0\0";
char troll4[] = u8"\nТролль издал душераздирающий крик, скорчил страшную рожу и скрылся в неизвестном\n"
u8"направлении с неправдоподобной скоростью\n\0";
char troll5[] = u8"\nТролль взял ваши денежки и, насвистывая мелодию пахабной песенки, пошел их\n"
u8"пропивать в ближайшую таверну.\n\0";
char troll13[] = u8"\nДенег у вас совсем не осталось, о чем вы честно признались грабителю. Трольь\n"
u8"обыскал вас, и, действительно не найдя денег, отвесил вам смачного пендуля.\n"
u8"В его понимании это значит что-то вроде ''извини, друг''...\n\0";
char meet_gold1[] = u8"◑\x0eВы нашли 21 золотой!\0";
char goblin1[] = u8"◑\x02Перед вами большой-пребольшой зеленый гоблин.\n"
u8"- Моя, эта, ленииивый-ленивый! Моя глодный! Моя умный! Твоя хотеть пройтить, а\n"
u8"  моя твоя не пускать, хе-хе, хе-хе! Твоя сперва дать моя кусок сала, моя твоя\n"
u8"  только тогда пускать, когда сытый, хе-хе! Моя ленивый-ленивый, однако, моя и\n"
u8"  умный-умный, хитрый-хитрый...\n\0";
char goblin2[] = u8"Вы решаете дать гоблину сало.        \0"
u8"Хрен ему, а не сало! Умник, тоже мне!\0\0";
char goblin3[] = u8"\nЕдинственная проблема - у вас нету с собой сала!\0";
char goblin4[] = u8"\n- Спасиба-спасиба! - сказал гоблин. Потом он заметил, как вы на него смотрите,\n"
u8"  и добавил - Твоя думать, моя её обмануть? Твоя плохо думать о моя! Моя честный!\n"
u8"После этого, рыгая и напевая пахабные песенки, гоблин ушел хрен знает куда.\0";
char meet_meat1[]  = u8"◑\x0fВы нашли увесистый шмат сала!\0";
char meet_water1[] = u8"◑\x09Вы стоите на берегу подземной реки. В темной холодной воде изредка проносятся\n"
u8"тени слепых рыб.\0";
char magican1[] = u8"◑\x0bВы встретили водного мага, опустившегося до того, чтобы начать продавать свои\n"
u8"волшебные свитки. Данный конкретный экземпляр предлагает вам купить у него\n"
u8"заклинание \"По Воде Аки Посуху\" по \"специальной цене\".\n"
u8"Имейте в виду, что маги отличаются способностью определять количество денег в\n"
u8"карманах своих клиентов, так что \"специальная цена\" скорее всего совпадёт с\n"
u8"этим самым количеством!\n\0";
char magican2[] = u8"Всё это очень хорошо, но вас не интересуют никакие заклининия.\0"
u8"Вы захотели купить этот свиток.                               \0\0";
char magican3[] = u8"Жалко, что у вас совсем нет денег!\0";
char magican4[] = u8"Как и следовало ожидать, маг посмотрел в волшебное зеркало, почесал репу и\n"
u8"Предложил вам не торгуясь оттдать все золото, какое у вас есть в кошельке.\n"
u8"Вы так и сделали... Маг отдал вам свиток и пошел пропивать заработок в ближайшую\n"
u8"таверну. Слава богу, он не догадался посмотреть, нет ли денег в ваших носках.\n"
u8"Кстати, именно это и отличает Жулика-Мага от Жулика-Тролля! Короче, у вас\n"
u8"остался ровно один золотой.\0";
char trader1[] = u8"◑\x02Перед вами гоблин. Как и все гоблины, он большой, зеленый и умный.\n"
u8"- Вах! А моя уж думать никто сюда не ходить! Моя зря расстроится! Слушай, моя\n"
u8"  предлагать твоя обмен! Моя раньшe менять шило на мыло. Мыло не вкусный! Моя\n"
u8"  умный-умный! Моя больше так не того! Моя теперя меняет лодка на сало! У твоя\n"
u8"  есть сало?\n\0";
char trader2[] = u8"- Нет, нету у меня сала!\0"
u8"- Давай менятся!        \0\0";
char trader3[] = u8"Вы отдали гоблину сало, он отдал вам лодку и, напевая такие песенки, от которых\n"
u8"у нормальных людей уши в трубочку сворачиваются, пошел жрать сало.\0";
char trader4[] = u8"- Нет, нету у меня сала!                       \0"
u8"- Давай менятся!                               \0"
u8"- Блин, а я все сало уже отдал твоему сородичу!\0\0";
char trader5[] = u8"Услышав это, гоблин издал радостный рык, схватил лодку под мышку и убежал.\0";
char trader6[] = u8"- Твоя сперва показать сало, моя потом давать лодка!\n"
u8"Cала у вас нет, так что ничего не получилось...\0";
char princess1[] = u8"◑\x0cПеред вами заколдованная принцесса Танагра.\n\0";
char princess2[] = u8"Вы целуете ее в губы.\0"
u8"Вы ничего не делаете.\0\0";
char princess3[] = u8"Вы целуете ее в губы.                 \0"
u8"Вы ничего не делаете.                 \0"
u8"Вы вливаете в нее весь спирт из фляги.\0\0";
char princess_kiss[] = u8"\nХмм... приятно... только вот она не проснулась.\0";
char princess_bottle[] = u8"\nПринцесса Танагра очнулась от дикого похмелья, встала с постели, поманила вас\n"
u8"к себе пальчиком, а, когда вы подошли поближе, чем-то со всей дури долбанула по\n"
u8"башке и вы отключились. Через час вы очнулись рядом с конделябром на полу.\0";
char duplo1[] = u8"◑\x0aВы стоите перед деревом с огромным дуплом.\n\0";
char duplo2[] = u8"Вы шарите в дупле рукой.                                     \0"
u8"Вы суете в дупло голову чтобы посмотреть, что там происходит.\0"
u8"Вы шуруете в дупле мечом.                                    \0"
u8"Ну его, это дупло, нафиг!                                    \0\0";
char duplo_0[] = u8"\nШарите...\n...шарите...\n...но ничего не нашариваете.\0";
char duplo_1[] = u8"\nНихрена не видно. Темно очень.\0";
char duplo_2[] = u8"\nПошуровав с полчаса вы убеждаетесь, что в дупле нету нихрена.\0";
char duplo_2_nsw[] = u8"\nТочнее, хотите пошуровать там мечом... А меча-то у вас и нету!\0";
char wiseman1[] = u8"◑\x0eСтарый мудрый отшельник в позе лотуса леветирует над камнями.\n"
u8"Он дико храпит. Сквозь храп иногда слышно что-то вроде:\n"
u8"  - Печеная картошка... соленый огурец... поллитра...  нет, литр водочки...\0";
char wiseman2[] = u8"◑\x0eМудрый старый отшельник в позе лотуса леветирует над камнями.\n"
u8"Он страшно храпит, и от него дико разит спиртным. Во сне он бормочет что-то до\n"
u8"того непристойное, что я не берусь вам это повторять.\0";
char wiseman3[] = u8"◑\x0eСтарый хрен зависает над камнями в позе лотуса. Он спит.\n"
u8"Во сне бормочет что-то о короле и его матушке, и что-то о вашей матушке тоже!!\0";
char wiseman4[] = u8"◑\x0eСтарая развалина храпит, с наглым видом зависая в воздухе. Слышно слова:\n"
u8"  - Дракона убить чтобы меч нужно в дупло окунуть того дуба, что растет!\n"
u8"    А зелье нужно расколдовать принцессу чтобы принести из лесу на юговостоке.\0";
char wiseman5[] = u8"◑\x0eСтарик храпит дальше:\n"
u8"  - Растет тот дуб на острове, что посреди озера, что питает ров замка, где\n"
u8"    спит принцесса, которую заколдовал злой маг, который умер год назад, падла!\0";
char bottle[] = u8"◑\x0bВы нашли флягу со спиртом!!!\0";
char done_princes[] = u8"◑\x0a  - Ну вот, ты и расколдовал принцессу!◑\x07 - раздался знакомый голос.\n"
u8"◑\x0f  - Да...\n"
u8"◑\x0a  - Что-то ты не особо доволен... что ж, хочешь, я открою тебе тайну?\n"
u8"◑\x0f  - Конечно!\0";
char done_dragon[] = u8"◑\x0f  - Йэеее!!! Я сразил зверюгу!!!\n"
u8"◑\x0a  - Тише, тшие, не кричи, не надо так шуметь в пещере!\n"
u8"◑\x0f  - А, это ты, Хранитель! Видал как я его?\0";
char done_both[] = u8"◑\x0f  - Эгеге-гей хранитель!?\n"
u8"◑\x0a  - О! Я вижу, ты уже все сделал! Молодец! Хорошая из тебя выйдет жаба!\n"
u8"◑\x0f  - Э!!! Хранитель!!! Какая жаба?! Мы же договорились!!!\0";
char wakeup[] = u8"\n◑\x07И вдруг вы проснулись...Да-да, это был только сон! Вы садитесь на кровати и\n"
u8"вдруг понимаете, что сжимаете что-то в руке. О, господи! Это же золотая монета!\n"
u8"Та самая золотая монета из сна, которую вы утаили от мага! Мистика!!!\0";

// E - КОРИЧНЕВАТАЯ СТЕНКА
// X - СТЕНКА

// . - ЧЕРНЫЙ ПОЛ
// - - ЗЕЛЕНЫЙ ПОЛ
// + - КОРИЧНЕВЫЙ ПОЛ

// * - ВОДА  нужна ЛОДКА для движения по воде
// G - ЗОЛОТО  21 ЗОЛОТОЙ
// M - САЛО
// u - СПИРТ

// W - ОТШЕЛЬНИК дает инструкции о ДУБЕ и СПИРТЕ

// d - ДУБ если сунуть в дупло МЕЧ, то он станет волшебным
// B - ГОБЛИН  берет САЛО и уходит
// D - ДРАКОН  ломает обычный МЕЧ // умирает от волшебного МЕЧА
// T - ТРОЛЛЬ  есть МЕЧ->уходит // берет всё ЗОЛОТО и уходит
// R - МАГ всё ЗОЛОТО->1 ЗОЛОТОЙ + ЗАКЛИНАНИЕ(ЛОДКА) + уходит
// L - МЕНЯЛА  информация о гоблине и САЛЕ->уходит // САЛО->ЛОДКА+уходит
// p - ПРИНЦЕССА для снятия заклятия нужен СПИРТ
char caveplan[] =
"XEXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXiiii1i1ii1i1ii1i11i1ii11111i1iiiiii"
"X.X....XXXXX...XXX...XXX.XXX.XXXXXXXXXXXXXXX----1---11i--i--1----i1--iiii11--i-i"
"X...**...XX.....DX.X.XX...X...XXXXXXXXXXXX---1--ii1--i-i-++++++++++++-1-----i--i"
"XX*****X....XXX....X.X..X.XXX....XXXXXXX-1---------------+-*****----+---****--ii"
"XXX***XXXLXXXXXXXX.....XX..XT.XX..X.XXXX----------1----**.***XX*****.****XX*---i"
"XXXX**XX....X..X.X.XXXXXXX...XXX.XX.XX-------i-----*****-+--*XXXXXXX+XXXXXX*i-i1"
"XXX**XXX.XX...X.....XX..XX.X..XM....X----1-------***1----+--**X++++++++++X**---i"
"XX**XX...TXXX..X.XX......XX.XXXXXXXXXXX----------*------++--i*XXXXXXX++++X*i-i11"
"X**XXXXXX.XXXX.XXX..XX............X----------i---**---+++----*X+++++X++++X*---ii"
"XX**X***X....XXX.....XXXXX.XX..XX.XXXX--1--------i**--+i----i*Xp+X+++++++X**i--i"
"XXX***X***.*..XXXX.X....X..XXX...XXX------------+++.+++-----*XXXXXXXXXXXXXX*---i"
"XXXXXXXX*****....XXXXXX.XXXX.XXX.B.XXXX----i----+--***-----1*XX**********XX*i-ii"
"XX..XXXXX***XXXX.X...X....X....XXX....XXX-------+---i**-----***i-----i--****-i-i"
"XX.XXXXXXXXXXX.....X..X.X....XGX.XX.X..X--------+-----**------------------i---11"
"XX....XXX......XX.XXXXX.X.XXXXXX....*XXXXXXXX--i+-----1**-----i-------------i--i"
"XXXX......X.XX.XX...XX..XXXXX..XX.***X**...XXX--+1------*-----------i--1-----1ii"
"XXXX.XXX.XXXXX.XXXX...XXX***....XX.****XXX....+++---i---*---i-----1--i---i--i--1"
"XXR...XX.XXXX..XXXXXXXXXX*XX...XXXXX**XXXXXXXX-----i---**----------------1i---ii"
"XX**.*XX..X......XX.XX****XXXXXX...X.XXX-i------i---1*****--1--i--i--i--i-i-i1ii"
"X*****XXX...XX.X.....**X*XX--XX..X...XX------1---1-***--****--1--i-1-1i-i-i1ii11"
"XX***XXXXX***X.XX.XXX*XXX-----XXXXXXXXXX---------****--d-****---i-1-i-ii---1--ii"
"XXX***XX*****..XX.X***XX-----------XXXX-i---1-i-******--****-i---i------1----ii1"
"XXXX****XX***X....XX***XX------------------------*1*******-i-i-i---i-1----i1---i"
"XXXXXXX****XXXX******XXXW--XX--XX-X-X-i----i-i--1ii1---ii-ii-i--i-ii-i11ii---iu1"
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXi1ii1i1i1i1ii11i11i1i1i1ii1i1ii1i1i11ii";
char caveseen[80*25];


void cls() {
  for (Si32 i = 0; i < 80*25; ++i) {
    Symbol &s = text_buf[i];
    s.code = (Ui32)' ';
    s.fore_color.rgba = 0xffffffff;
    s.back_color.rgba = 0;
  }
  text_fore_color.rgba = 0xffffffff;
  text_back_color.rgba = 0;
  text_x = 0;
  text_y = 0;
}

Rgba decode_color(Ui32 color_code) {
  Ui32 bright = (color_code & 0x08) ? 128 : 0;
  Ui32 r = (color_code & 0x04) ? 127 : 0;
  Ui32 g = (color_code & 0x02) ? 127 : 0;
  Ui32 b = (color_code & 0x01) ? 127 : 0;
  return Rgba(bright + r, bright + g, bright + b, 255);
}
// control character is 0x25d1 or u8"◑"
const char * print(const char *text) {
  Utf32Reader reader;
  reader.Reset((const Ui8 *)text);
  for (Ui32 ch = reader.ReadOne(); ch != 0; ch = reader.ReadOne()) {
    if (ch == 0x25d1) {
      Ui32 color_code = reader.ReadOne();
      text_fore_color = decode_color(color_code);
    } else if (ch == '\n') {
      text_x = 0;
      text_y++;
    } else {
      Si32 pos = (text_y * 80 + text_x) % (80 * 25);
      Symbol &s = text_buf[pos];
      s.code = ch;
      s.fore_color = text_fore_color;
      s.back_color = text_back_color;
      text_x++;
    }
  }
  return (const char *)reader.p;
}

void present() {
  Clear();
  Sprite backbuffer = GetEngine()->GetBackbuffer();
  Sprite full = g_ch[0x2588]; //u8"█"
  
  for (Si32 i = 0; i < 80*25; ++i) {
    Symbol &s = text_buf[i];
    Si32 x = i % 80 * 8;
    Si32 y = ScreenSize().y - i / 80 * 16 - 16;
    full.Draw(backbuffer,
      x, y, kDrawBlendingModeColorize, kFilterNearest, s.back_color);
    g_ch[s.code].Draw(backbuffer,
      x, y, kDrawBlendingModeColorize, kFilterNearest, s.fore_color);
  }
  ShowFrame();
}

void inputef(char *out_text) {
  Si32 len = 0;
  out_text[0] = 0;
  ShowFrame();
  
  while (!IsKeyDownward(kKeyEscape)) {
    Clear();
    for (Si32 idx = 0; idx < InputMessageCount(); ++idx) {
      const InputMessage &m = GetInputMessage(idx);
      Ui32 ch = 0;
      if (m.kind == InputMessage::kKeyboard && m.keyboard.key_state == 1) {
        if ((m.keyboard.key >= kKey0 && m.keyboard.key <= kKey9)
            || (m.keyboard.key >= kKeyA && m.keyboard.key <= kKeyZ)
            || (m.keyboard.key == kKeySpace)) {
              ch = m.keyboard.key;
        } else if (m.keyboard.key >= kKeyNumpad0 && m.keyboard.key <= kKeyNumpad9) {
          ch = m.keyboard.key - kKeyNumpad0 + kKey0;
        } else if (m.keyboard.key == kKeyEnter) {
          if (len) {
            out_text[len] = 0;
            return;
          }
        } else if (m.keyboard.key == kKeyBackspace) {
          if (len) {
            out_text[len] = 0;
            len--;
            out_text[len] = ' ';
          }
        }
        if (ch != 0) {
          if (len < 25) {
            out_text[len] = ch;
            len++;
            out_text[len] = 0;
          }
        }
      }
    }
    Si32 tx = text_x;
    print(out_text);
    text_x = tx;
    present();
  }
}

Si32 choose(const char *text) {
  Si32 selection = 0;
  while (true) {
    const char *nextText = text;
    Si32 line = 0;
    while (true) {
      Si32 tx = text_x;
      nextText = print(nextText);
      text_x = tx;
      
      if (selection == line) {
        break;
      }
      line++;
      nextText++;
      if (*nextText == 0) {
        nextText = text;
        selection = 0;
        line = 0;
      }
    }
    
    present();
    if (IsKeyDownward(kKeyEscape)) {
      return 0;
    }
    if (IsKeyDownward(kKeyEnter)) {
      return selection;
    }
    if (IsAnyKeyDownward()) {
      selection++;
    }
  }
}

void wait_space() {
  if (IsKeyDownward(kKeyEscape)) {
    return;
  }
  present();
  while (!IsKeyDownward(' ') && !IsKeyDownward(kKeyEnter)) {
    if (IsKeyDownward(kKeyEscape)) {
      return;
    }
    ShowFrame();
  }
  ShowFrame();
}

void print_and_wait(const char *text) {
  print(text);
  wait_space();
}

void paint_cave() {
  Ui32 color = 0;
  for (Si32 bx = 0; bx < 80 * 25; ++bx) {
    if (caveseen[bx] == 0) {
      color = 0;
    } else {
      switch (caveplan[bx]) {
        case 'X': color = 0x07db; break; //wall white
        case '.': color = 0x00db; break; //land black
        case 'D': color = 0x0c02; break; //dragon red
        case 'T': color = 0x0a02; break; //green troll
        case 'G': color = 0x0e0f; break; //yellow gold
        case 'E': color = 0x74b1; break; //white-brown Entrance
        case 'B': color = 0x0201; break; //dark-green Goblin
        case 'M': color = 0x0f07; break; //white Meat
        case '*': color = 0x19b1; break; //blue water
        case 'R': color = 0x0b01; break; //blue magican
        case 'L': color = 0x0201; break; //dark-green Goblin-menyala
        case '-': color = 0x02db; break; //green grass
        case '+': color = 0x04db; break; //brown road
        case 'i': color = 0x2a06; break; //brightgreen on darkgreen tree2
        case 'd': color = 0x2a06; break; //brightgreen on darkgreen tree2
        case '1': color = 0x2a06; break; //brightgreen on darkgreen tree2
        case 'p': color = 0x4c03; break; //red on brown princess
        case 'W': color = 0x2e01; break; //yellow on green wiseman
        case 'u': color = 0x2b04; break; //blue on green bottle
        default: color = 0x0f00 + caveplan[bx]; break;
      }
    }
    
    // draw color at bx
    Si32 pos = bx;
    Symbol &s = text_buf[pos];
    s.code = g_ascii[color & 0xff];
    s.fore_color = decode_color(color >> 8);
    s.back_color = decode_color(color >> 12);
  }
  // draw player at cave_pos, char 0x01, color player_background
  Symbol &s = text_buf[cave_pos];
  s.code = g_ascii[0x01];
  s.fore_color = decode_color(player_background);
  s.back_color = decode_color(player_background >> 4);
}

void meet_dragon() {
  cls();
  print(dragon_1);
  if (have_bad_sword == 0) {
    print_and_wait(dragon_2_1);
  } else {
    Si32 dx = choose(dragon_2);
    ++text_y;
    if (dx < 1) { // hit him with your sword
      if (sword_good == 1) {
        print_and_wait(dragon_die);
        caveplan[new_cave_pos] =  '.';
        have_killed = 1;
      } else {
        print_and_wait(dragon_3);
        have_bad_sword = 0;
      }
    }
  }
}

void meet_troll_0() {
  print_and_wait(troll4);
  caveplan[new_cave_pos] = '.';
}
void meet_troll_1() {
  caveplan[new_cave_pos] = '.';
  print_and_wait(troll5);
  have_gold = 0;
}
void meet_troll() {
  cls();
  print(troll1);
  if (have_bad_sword == 1) {
    if (have_gold >= 1) {
      Si32 dx = choose(troll2_1);
      ++text_y;
      if (dx == 1) {
        meet_troll_0();
      } else {
        // have 2 give gold
        meet_troll_1();
      }
    } else {
      print(troll2);
      ++text_y;
      meet_troll_0();
    }
  } else if (have_gold >= 1) {
    print(troll2_1);
    ++text_y;
    meet_troll_1();
  } else {
    print_and_wait(troll13);
  }
}

void meet_gold() {
  caveplan[new_cave_pos] = '.';
  cls();
  have_gold = 21;
  print_and_wait(meet_gold1);
}

void meet_goblin() {
  cls();
  print(goblin1);
  Si32 dx = choose(goblin2);
  if (dx < 1) {
    if (have_meat < 1) {
      print_and_wait(goblin3);
    } else {
      have_meat = 0;
      gave_meat = 1;
      print_and_wait(goblin4);
      caveplan[new_cave_pos] = '.';
    }
  }
  // donothing
}

void meet_meat() {
  caveplan[new_cave_pos] = '.';
  cls();
  have_meat += 1;
  print_and_wait(meet_meat1);
}

void meet_water() {
  cls();
  print_and_wait(meet_water1);
}

void meet_magican() {
  cls();
  print(magican1);
  Si32 dx = choose(magican2);
  if (dx == 1) {
    ++text_y;
    if (have_gold > 0) {
      have_gold = 1;
      have_boat = 1;
      print_and_wait(magican4);
      caveplan[new_cave_pos] = '.';
    } else {
      print_and_wait(magican3);
    }
  }
}

void meet_trader3() {
  if (have_meat == 1) {
    print_and_wait(trader3);
    caveplan[new_cave_pos] = '.';
    have_meat = 0;
    have_boat = 1;
  } else {
    print_and_wait(trader6);
  }
}
void meet_trader() {
  cls();
  print(trader1);
  if (gave_meat == 1) {
    Si32 dx = choose(trader4);
    ++text_y;
    if (dx < 1) {
      return;
    } else {
      if (dx == 1) {
        meet_trader3();
      } else {
        print_and_wait(trader5);
        caveplan[new_cave_pos] = '.';
      }
    }
  } else {
    Si32 dx = choose(trader2);
    ++text_y;
    if (dx == 1) {
      meet_trader3();
    }
  }
}

void meet_princess() {
  cls();
  print(princess1);
  if (have_bottle == 1) {
    Si32 dx = choose(princess3);
    if (dx < 1) {
      print_and_wait(princess_kiss);
    } else if (dx > 1) {
      print_and_wait(princess_bottle);
      have_spelled = 1;
      caveplan[new_cave_pos] = '+';
    }
  } else {
    Si32 dx = choose(princess2);
    if (dx < 1) {
      print_and_wait(princess_kiss);
    }
  }
}

void meet_duplo() {
  cls();
  print(duplo1);
  Si32 dx = choose(duplo2);
  if (dx < 1) {
    print_and_wait(duplo_0);
  } else if (dx == 1) {
    print_and_wait(duplo_1);
  } else if (dx == 2) {
    if (have_bad_sword == 0) {
      print_and_wait(duplo_2_nsw);
    }
    print_and_wait(duplo_2);
    sword_good = 1;
  }
}

void meet_wiseman() {
  cls();
  ++wiseman_visitnum;
  if (wiseman_visitnum == 1) {
    print_and_wait(wiseman1);
  } else if (wiseman_visitnum == 2) {
    print_and_wait(wiseman2);
  } else if (wiseman_visitnum == 3) {
    print_and_wait(wiseman3);
  } else if (wiseman_visitnum == 4) {
    print_and_wait(wiseman4);
  } else {
    wiseman_visitnum = 0;
    print_and_wait(wiseman5);
  }
}

void meet_bottle() {
  caveplan[new_cave_pos] = '-';
  cls();
  print_and_wait(bottle);
  have_bottle = 1;
}

void check_on_done() {
  if (have2_kill != 1) {
    if (have_spelled == 1) {
      game_done = 1; //princess only!!
      cls();
      print_and_wait(done_princes);
    }
  } else if (have2_spell != 1) {
    if (have_killed == 1) {
      game_done = 1; //dragon only!!
      cls();
      print_and_wait(done_dragon);
    }
  } else if (have_killed == 1 && have_spelled == 1) {
    game_done = 1; // princess and dragon!!!
    cls();
    print_and_wait(done_both);
  }
}


void make_move_move() {
  cave_pos = new_cave_pos;
  caveseen[80 + new_cave_pos] = 1;
  caveseen[new_cave_pos] = 1;
  caveseen[new_cave_pos + 1] = 1;
  caveseen[new_cave_pos - 1] = 1;
  caveseen[-80 + new_cave_pos] = 1;
  paint_cave();
}

void make_move() {
  switch (caveplan[new_cave_pos]) {
    case 'X': break;
    case '.':
      player_background = 0x0f;
      make_move_move();
      break;
    case 'D': meet_dragon(); break;
    case 'T': meet_troll(); break;
    case 'G': meet_gold(); break;
    case 'B': meet_goblin(); break;
    case 'M': meet_meat(); break;
    case '*':
      if (have_boat == 1) {
        player_background = 0x9f;
        make_move_move();
      } else {
        meet_water();
      }
      break;
    case 'R': meet_magican(); break;
    case 'L': meet_trader(); break;
    case '-':
      player_background = 0x2f;
      make_move_move();
      break;
    case '+':
      player_background = 0x4f;
      make_move_move();
      break;
    case 'p': meet_princess(); break;
    case 'd': meet_duplo(); break;
    case 'W': meet_wiseman(); break;
    case 'u': meet_bottle(); break;
  }
}

void in_cave_keystroke_analyzer() {
  if (IsKeyDownward(kKeyDown)) {
    new_cave_pos = cave_pos + 80;
    make_move();
  } else if (IsKeyDownward(kKeyUp)) {
    new_cave_pos = cave_pos - 80;
    make_move();
  } else if (IsKeyDownward(kKeyLeft)) {
    new_cave_pos = cave_pos - 1;
    make_move();
  } else if (IsKeyDownward(kKeyRight)) {
    new_cave_pos = cave_pos + 1;
    make_move();
  }
}

void EasyMain() {
  ResizeScreen(640, 400);
  SetFullScreen(true);
  SetVSync(true);
  
  // Load font
  Sprite font;
  font.Load("data/font_vga_rus.tga");
  g_ascii.resize(vec.size());
  for (Si32 idx = 0; idx < vec.size(); ++idx) {
    Si32 x = idx % 16;
    Si32 y = 15 - idx / 16;
    const Ui8* p = (const Ui8*)vec[idx].data();
    Utf32Reader reader;
    reader.Reset(p);
    Ui32 u = reader.ReadOne();
    Sprite tmp;
    tmp.Reference(font, x * 8, y * 16, 8, 16);
    g_ch[u].Clone(tmp);
    g_ch[u].UpdateOpaqueSpans();
    g_ascii[idx] = u;
  }
  
  // Initialize game variables
  for (Si32 i = 0; i < 80*25; ++i) {
    caveseen[i] = 0;
  }
  thy_name[0] = 0;
  player_background = 0x0f;
  new_cave_pos = 81;
  
  // Mission select
  cls();
  print(welcome);
  text_x = 12;
  inputef(thy_name);
  if (IsKeyDownward(kKeyEscape)) {
    return;
  }
  text_x = 0;
  print(text2);
  print(thy_name);
  print(text3);
  Si32 dx = choose(text4);
  text_y++;
  if (dx < 1) {
    have2_kill = 1;
  } else if (dx == 1) {
    have2_spell = 1;
  } else {
    //target2
    print(text5);
    print(thy_name);
    print(text6);
    have2_kill = 1;
    have2_spell = 1;
    text_y++;
  }
  print_and_wait(text7);
  if (IsKeyDownward(kKeyEscape)) {
    return;
  }
  
  // Game start
  make_move();
  while (!IsKeyDownward(kKeyEscape)) {
    paint_cave();
    check_on_done();
    if (game_done == 1) {
      print_and_wait(wakeup);
      break;
    }
    in_cave_keystroke_analyzer();
    if (IsKeyDownward(kKeyEscape)) {
      break;
    }
    paint_cave();
    present();
  }
}
